package com.quest;

import org.springframework.stereotype.Component;

public class TreasureQuest implements Quest {
    private String name;
    private Integer level;

    public void start() {
        // Treasure specific implementation

        System.out.print("This is Treasure Quest");
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
