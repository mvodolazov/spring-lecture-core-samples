package com;

import com.knight.Knight;
import com.quest.Quest;
import com.quest.TreasureQuest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
    @Bean
    public Quest quest(){
        return new TreasureQuest();
    }

    @Bean
    public Knight knight(){
        Knight knight = new Knight();
        knight.setQuest(quest());
        return knight;
    }
}
