package com.knight;

import com.quest.Quest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

public class Knight {

    private Quest quest;

    public void startQuest() {
        quest.start();
    }

    public void setQuest(Quest quest) {
        this.quest = quest;
    }
}
